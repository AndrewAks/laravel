<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

	# Функция getUser обеспечивает вывод списка всех зарегистрированнх в системе пользователей.
    public function getUser(){
        $users = User::all();
        return $users;
    }
    
	# Список полей в таблице `users`, подлежащих записи.
    protected $fillable = [
        'name', 'email', 'password', 'status', 'gruppa', 'avatar', 'reg_ip',
    ];

    # Список скрытых в интересах безопасности полей.
    protected $hidden = [
        'password', 'remember_token',
    ];
}
