<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Post;
use App\Models\Upload;
use App\Models\Students;
use DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;

class MyController extends Controller{
	# Коробочная функция, обеспечивающая работу авторизации.
	public function __construct(){
		$this->middleware('auth');
	}
	
	# Функция index обеспечивает формирование страницы с собственными загрузками пользователя.
	public function index(Post $post, Students $students, Upload $upload, $doctype='any', $pagination=1){
		#pagination fix
		if(($doctype != "file") and ($doctype != "image") and ($doctype != "video") and ($doctype != "any")): $pagination = $doctype; $doctype = 'any'; endif;

 	   $itemsPerPage = 25;
		$users = new User;
		$user = $users->getUser();
		# Определяем, является ли пользователь преподавателем.
		if(Auth::user()->status == "teacher"){
			# Устанавливаем, что преподаватель загружает файлы не только в свой профиль, поэтому их тоже нужно отображать.
			$scanUploads = $upload->displayUploadsByOriginatingId(Auth::user()->id, $doctype, $pagination, $itemsPerPage);
		}else{
			# Выбираем только те файлы, которые были загружены пользователем в собственную учетную запись.
			$scanUploads = $upload->displayUploadsById(Auth::user()->id, $doctype, $pagination, $itemsPerPage);
		}
		# Получаем общее количество загрузок, удовлетворяющих нашим требованиям.
		$summaryCount = $upload->summaryCount(Auth::user()->status, Auth::user()->id, $doctype);
		# Получаем аватары и имена пользователей для отображения вместо идентификаторов.
		$userNames = $students->getUsernames();
		# Считаем количество отображаемых документов на данной конкретной странице.
		$postCount = count($scanUploads->all());
		# Считаем количество страниц, на которое можно разбить вывод.
		$pagesInSection = ceil($summaryCount/$itemsPerPage);
		return view('my')
			->with('postCount', $postCount)
			->with('scanUploads', $scanUploads)
			->with('userNames', $userNames)
			->with('users', $user)
			->with('summaryCount', $summaryCount)
			->with('itemsPerPage', $itemsPerPage)
			->with('rqDocType', $doctype)
			->with('pagination', $pagination)
			->with('pagesInSection', $pagesInSection);
	}

	public function export() {
	    $user = Auth::user();
        $filename = $this->createFile($user);
        return response()->download($filename)->deleteFileAfterSend(true);
    }

    private function createFile($user) {
        $templateProcessor = new TemplateProcessor(__DIR__ . '/../../../template.docx');
        $templateProcessor->setValue('${fio}', $user->name);
        $templateProcessor->setValue('${email}', $user->email);
        $templateProcessor->setValue('${date}', $user->created_at);
        $templateProcessor->setValue('${status}', $this->getStatus($user->status));
        $templateProcessor->saveAs($user->id . '.docx');
        return $user->id . '.docx';
    }

    private function getStatus($status) {
	    $statuses = [
	        'teacher' => 'Преподаватель',
            'monitor' => 'Староста',
            'student' => 'Студент'
        ];
	    return $statuses[$status];
    }
}
