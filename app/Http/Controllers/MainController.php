<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\User;
use App\Models\Kafs;
use App\Models\Facultets;
use App\Models\Groups;
use App\Models\Upload;
use App\Models\Students;
use Intervention\Image\Facades\Image;
use Validator;
use Auth;
use DB;

class MainController extends Controller
{
	/*
	 Функция index обеспечивает вывод заглавной страницы профиля пользователя в полном объеме, учивая при этом статс пользователя в системе. Результатом работы данной функции является передача в соответствующее представление полностью готового блока данных, на основании которого происходил формирование информации, отображаемой пользователю. Поскольку на данной странице осуществляется вывод только последних десяти загруженных файлов либо документов, постраничная навигация для данного представления не предусматривается.
	*/
	public function index(Post $posts, Kafs $kafs, Facultets $facultets, Groups $groups, Students $students, Upload $upload){
		$post = $posts->getPost(0);
		/*
		 Если у пользователя не задана в профиле группа (место в структуре университета), то мы выводим для него в полной мере список всех кафедр, групп и факультетов, чтобы он определился со своим местом в этой жизни.
		*/
		if(Auth::user()->gruppa == "none"){
			$kaf  = $kafs->getKafs();
			$facultet = $facultets->getFacultets();
			$group = $groups->getGroups();
		}else{
			if(Auth::user()->status == "teacher"){
				/*
				 Если пользователь имеет статус преподавателя, то для него мы вибираем только кафедру, к которой он привязан, и данные о факультете. Поскольку данных о группе в данном случае быть не может, мы устанавливаем значение в 'none'. Эта информация выводится в данных о месте пользователя в структуре университета и больше нигде не используется.
				*/
			     $kaf  = $kafs->getKaf(Auth::user()->gruppa);
			     $facultet = $facultets->getFacultet($kaf->facultet_id);
			     $group = 'none';
			}else{
				/*
				 Для пользователей в иных статусах мы вытягиваем всю информацию, поскольку и студент и староста привязаны к конкретной группе, а не к факультету или кафедре.
				*/
				 $group = $groups->getGroup(Auth::user()->gruppa);
			     $kaf  = $kafs->getKaf($group->kafedra_id);
			     $facultet = $facultets->getFacultet($kaf->facultet_id);
			}
		}
		# Получаем список подконтрольных групп на основании данных из профиля преподавателя.
		if((Auth::user()->status == "teacher") and (Auth::user()->gruppa != "none")){
			# Получаем список всех групп, относящихся к кафедре, к которой привязан преподаватель.
			$grpByKaf = $groups->getGroupsByKaf(Auth::user()->gruppa);
			if(count($grpByKaf)>0){
				/*
				 Получаем список подконтрольных групп для данного преподавателя на основании поля teacher_groups из профиля преподавателя.
				*/
				$teacherGroups = $groups->getTeacherGroups(Auth::user()->teacher_groups);
				/*
				 Разделяем список подконтрольных групп и превращаем его в массив для последющей передачи в функцию getStudentsByKaf
				*/
				$tgroupsArr = explode(',',Auth::user()->teacher_groups);
				# Выбираем список студентов из подконтрольных преподавателю групп
				$grpStudents = $students->getStudentsByKaf($tgroupsArr);
			}
		}elseif(Auth::user()->status == "monitor"){
			# Выбираем список студентов из той же группы, к которой относится староста.
			$grpStudents = $students->getStudentsByGroup(Auth::user()->gruppa);
		}
		/*
		 Если список студентов пуст, то прибавляем в список пользователей для данного вывода пользователя, для которого данные формируются, чтобы он мог загружать файлы и документы от своего имени в свой собственный профиль.
		*/
		if((!isset($grpStudents)) or (count($grpStudents)<1)){
			$grpStudents = $students->getStudent(Auth::user()->id);
		}
		/*
		 Если группы в кафедре отсутствуют, то дальше нам нет смысла производить с ними какие-либо манипуляции, поэтому просто укажем явно, что этого делать не стоит во избежание runtime-ошибок.
		*/
		if(!isset($grpByKaf)){
			$grpByKaf = 'none';
			$slaveGroups = 'none';
		}else{
			/*
			 В данном фрагменте формируется список, при помощи которого преподаватель может редактировать привязанные группы. Механизм выводит все группы, находищеся на кафедре, к которой привязан преподаватель, а такеж выделяет из них те, которые уже установлены в качестве подотчетных для того, чтобы не было необходимости каждый раз выбирать весь список заново.
			*/
			foreach($grpByKaf as $kafGr){
				$checkSelected = $posts->ifInTeacherGroups($kafGr->id);
				$slaveGroups[$kafGr->id] = $checkSelected;
			}
		}
		# Получаем список загруженных документов
		if(Auth::user()->status == "teacher"){
			/*
			 Если страницу загружает преподаватель, то ему показываем все документы, которые загружались им независимо от того, была это загрузка в собственный профиль или же в профиль какого-либо иного пользователя.
			*/
			$scanUploads = $upload->getUploadsByOriginatingId(Auth::user()->id);
		}else{
			/*
			 Для студентов и старост выбираем только те документы, которые были загружены в их собственные профили, в том числе и преподавателями от их имени.
			*/
			$scanUploads = $upload->getUploadsById(Auth::user()->id);
		}
		# Получаем имена пользователей для того, чтобы отображать их вместо идентификаторов.
		$userNames = $students->getUsernames();
		# Считаем общее количество документов, которые будут показаны на странице.
		$postCount = count($scanUploads->all());
		# Если у преподавателя нет ни одной подконтрольной группы, то больше не затрагиваем эту тему.
		if(!isset($teacherGroups)):	$teacherGroups = 'none'; endif;
		if(!isset($slaveGroups)): $slaveGroups = 'none'; endif;
		# Передача сформированной функцией информации в представление.
		return view('profile')
			->with('post',$post)
			->with('postCount', $postCount)
			->with('kaf',$kaf)
			->with('facultet',$facultet)
			->with('group',$group)
			->with('students',$grpStudents)
			->with('teacherGroups',$teacherGroups)
			->with('groupByKaf',$grpByKaf)
			->with('slaveGroups',$slaveGroups)
			->with('scanUploads',$scanUploads)
			->with('userNames',$userNames);
	}

	/*
	 Функция setGroup исполняет обязанности обработчика POST-запроса на установку пользователем места в структуре университета.
	*/
    public function setGroup(Post $post, Request $request){
        $post->create($request->all());
        return redirect()->back();
    }

	/*
	 Функция update_avatar обеспечивает загрузку и обновление аватара пользователя средствами личного кабинета.
	*/
    public function update_avatar(Request $request){

        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename= time(). '.' . $avatar->getClientOriginalExtension();
			# Урезаем картинку до 300x300 и записываем в папку /img/avatars.
            Image::make($avatar)->resize(300, 300)->save( public_path('/img/avatars/'. $filename ));
			# Записываем адрес нового аватара в профиль пользователя
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        return redirect()->back();
    }
	
	/*
	 Функция loadDocument описывает механизм загрузки файлов и документов в систему. Скриптом определяется тип загружаемого файла либо документа, проверяется его соответствие указанным в форме данным и выполняется запись полученных данных в соответствующую таблицу базы данных.
	*/
	public function loadDocument(Request $request){
		# Проверяем, задано ли наименование загружаемого документа.
		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
		]);
		# Если в форме был указан загружаемый файл...
        if ($request->hasFile('document')){
			$document = $request->file('document');
			# ...то мы получим его расширение, чтобы делать выводы на его основании.
			$ext = $document->getClientOriginalExtension();
			# Если в форме был указан тип данных "Графический документ"...
			if($request->input('doctype') == "image"){
				if(($ext != "jpg") and ($ext != "jpeg") and ($ext != "png") and ($ext != "gif")){
					/*
					 ...а приложил пользователь что-то совершенно другое, то такой запрос мы не примем и файл загружать не будем.
					*/
					return redirect()->back();
				}else{
					/*
					 ...и все нормально, то мы запишем файл на сервер, созданим для него миниатюру размером 300x300 (чтобы не подгружать на страницу фотографии на 16Мп, например).
					*/
					$fileStub = time();
					$filename= $fileStub. '.' . $document->getClientOriginalExtension();
					$filenameFull= $fileStub. '_full.' . $document->getClientOriginalExtension();
					Image::make($document)->resize(300, 300)->save( public_path('uploads/'. $filename ));
					Image::make($document)->save( public_path('uploads/'. $filenameFull ));
				}
			}elseif($request->input('doctype') == "video"){
				# Если в форме был указан тип данных "Видео"...
				if(($ext != "flv") and ($ext != "mp4") and ($ext != "mpg") and ($ext != "mpeg") and ($ext != "3gp")){
					/*
					 ...а приложил пользователь что-то совершенно другое, то такой запрос мы не примем и файл загружать не будем.
					*/
					return redirect()->back();
				}else{
					/*
					 ...и все нормально, то мы прото сохраним это видео в публичной папке на сервере, больше с ним ничего не нужно делать.
					*/
					$fileStub = md5(time());
					$path = public_path().'/uploads/';
					$document->move($path, $fileStub.'.'.$ext);
				}
			}elseif($request->input('doctype') == "file"){
				# Если в форме был указан тип данных "Файл"...
				if(($ext != "doc") and ($ext != "docx") and ($ext != "xls") and ($ext != "xlsx") and ($ext != "ppt") and ($ext != "txt") and ($ext != "pdf") and ($ext != "zip") and ($ext != "rar")){
					/*
					 ...а приложил пользователь что-то совершенно другое, то такой запрос мы не примем и файл загружать не будем.
					*/
					return redirect()->back();
				}else{
					/*
					 ...и все нормально, то такий файл мы запишем в недоступную извне папку на сервере, поскольку в противном случае мы откроем уязвимость, которая позволила бы загружать вредоносный код и удаленного его исполнять, запуская процедуру посредством GET- или POST-запроса. Исходные имена файлов также не сохраняются в интересах безопасности.
					*/
					$fileStub = md5(time());
					$path = '/var/www/laravel/uploads/';
					$document->move($path, $fileStub.'.'.$ext);
				}
			}
			$uplMech = (new Upload);
			# Производим запись полученной информации в базу данных.
			$uplMech->create([
				'title' => $request->input('title'),
				'description' => $request->input('description'),
				'type' => $request->input('doctype'),
				'filename' => $fileStub,
				'filetype' => $document->getClientOriginalExtension(),
				'owner_id' => $request->input('owner_id'),
				'originating_owner' => Auth::user()->id,
				'viewable' => $request->input('viewable'),
			]);
        }
        return redirect()->back();
    }

	/*
	 Функция setTeacherGroups устанавливает список подконтрольных преподавателю групп, получая его посредством POST-запроса из соответствующей формы в виде массива и преобразуя в установленный строчный формат.
	*/
	 public function setTeacherGroups(Request $request){
	 	$teacherGroups = $request->get('teacherGroups');
	 	$countGroups = count($teacherGroups);
	 	$groupWrite = '';
	 	for($thsGroup=0;$thsGroup<$countGroups;++$thsGroup){
	 		$groupWrite .= $teacherGroups[$thsGroup];
	 		if($thsGroup != ($countGroups-1)){
	 			$groupWrite .= ',';
	 		}
	 	}
		DB::table('users')
         ->where('id', Auth::user()->id)
         ->update(['teacher_groups' => $groupWrite]);
      return redirect()->back();
	 }
	 
	/*
	 Функция remove реализует удаление загруженных файлов с сервера. В данном случае, реализуется только физическое удаление самого контента, соответствующие вхождения в базе данных данной функцией не затрагиваются, информация об успешном удалении лишь передается другой функции.
	*/
	 public function remove($identifier, Upload $upload){
        $rqFile = $upload->getFileById($identifier);
		# Сравниваем, равен ли id владельца файла id пользователя, запросившего удаление.
        if($rqFile->owner_id == Auth::user()->id){
			# Поскольку для разных типов данных различаются пути, то мы это учитываем в коде ниже.
			if($rqFile->type == "video"){
        		unlink('/var/www/laravel/public/uploads/'.$rqFile->filename.'.'.$rqFile->filetype);
        	}elseif($rqFile->type == "image"){
        		unlink('/var/www/laravel/public/uploads/'.$rqFile->filename.'.'.$rqFile->filetype);
        		unlink('/var/www/laravel/public/uploads/'.$rqFile->filename.'_full.'.$rqFile->filetype);
       		}elseif($rqFile->type == "file"){
       			unlink('/var/www/laravel/uploads/'.$rqFile->filename.'.'.$rqFile->filetype);
       		}
			/*
			 Сообщаем функции removeFile о том, что файл физически удален и теперь можно удалить соответствующие ему вхождения в базе данных.
			*/
       		$upload->removeFile($identifier);
        }else{
        		#access violation
        }
        return redirect()->back();
    }
}
