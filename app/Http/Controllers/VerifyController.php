<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Students;
use Auth;
use DB;

class VerifyController extends Controller{
	# Коробочная функция, обеспечивающая работу системы авторизации.
	public function __construct(){
		$this->middleware('auth');
	}
	
	# Функция index обеспечивает вывод страницы со списком пользователей, ожидающих верификации регистраций.
	public function index(Students $students, $pagination=1){
		# На странце выводится не более 25 пользователей.
		$itemsPerPage = 25;
		# Получаем список полдьзователей из очереди на верификацию.
		$unverified = $students->getUnverified($pagination, $itemsPerPage);
		# Считаем количество пользователей, отданных базой данных по этому запросу.
		$unverifiedQ = $unverified->count();
		# Получаем суммарную длину очереди пользователей на верификацию.
		$unverifiedSum = $students->getUnverifiedSum();
		# Считаем количество страниц, на которое может быть разбит вывод.
		$pagesInSection = ceil($unverifiedSum/$itemsPerPage);
		return view('verify')
			->with('unverified', $unverified)
			->with('unverifiedQ', $unverifiedQ)
			->with('pagination', $pagination)
			->with('itemsPerPage', $itemsPerPage)
			->with('pagesInSection', $pagesInSection)
			->with('unverifiedSum', $unverifiedSum);
	}
	
	# Функция accept обеспечивает одобрение заявки на регистрацию учетной записи преподавателя со стороны администратора ресурса.
	public function accept(Students $students, $user_id){
	 # Проверяем, имеет ли пользователь право на одобрение заявок.
     if(Auth::user()->is_admin == 1){
			# Исполняем запрос на успешное одобрение регистрации.
     		$students->verifyUser($user_id);
     }
     return redirect()->back();
	}
	
	# Функция decline обеспечивает отклонение запроса на регистрацию учетной записи преподавателем со стороны администратора ресурса.
	public function decline(Students $students, $user_id){
	 # Проверяем, имеет ли пользователь право на отклонение заявки
     if(Auth::user()->is_admin == 1){
			# Передаем запрос на отклонение регистрации.
     		$students->declineUser($user_id);
     }
     return redirect()->back();
	}
}
