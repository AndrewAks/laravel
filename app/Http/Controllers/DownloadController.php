<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Post;
use App\Models\Upload;
use DB;

class DownloadController extends Controller
{
    # Коробочная функция, обеспечивающая работу авторизации.
    public function __construct()
    {
        $this->middleware('auth');
    }

    # Функция index обеспечивает безопасное скачивание с сервера загруженного пользователем файла.
    public function index($fileid, Upload $upload)
    {
			# Получаем данные о запрошенном файле.
			$getFile = $upload->getFile($fileid);
			# Строим путь к файлу в месте его хранения на нашем сервере.
			$filetoget = '/var/www/laravel/uploads/'.$getFile->filename.'.'.$getFile->filetype;
			/*
			 Передаем в браузер пользователя заголовки, которые позволяют скачать файл, а не просмотреть его непосредственно в окне браузера в виде исходного кода.
			*/
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header("Accept-Ranges: bytes");
			header("Content-Length: ".filesize($filetoget));
			header('Content-Disposition: attachment; filename="'.str_replace(array('\\', '/', '"', '.'), '', $getFile->title).'.'.$getFile->filetype.'"');
			/*
			 Читаем файл и передаем его в браузер пользователя. Таким образом обеспечивается невозможность удаленного исполнения пользовательских файлов на сервере, что было бы достаточно серьезной дырой в безопасности.
			*/
			readfile($filetoget);
			# Когда файл будет передан, просто завершаем соединение. Вывод для данного случая не требуется.
			exit();
    }
}
