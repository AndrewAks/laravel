<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Upload;
use App\Models\Groups;
use App\Models\Students;
use App\Models\Post;
use Auth;
use DB;

class HomeController extends Controller
{
	# Коробочная функция, обеспечивающая работу авторизации.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
	 Функция index обеспечивает формирование домашней страницы проекта (Ленты файлов), на которой отображаются последние загрузки пользователей с учетом их области видимости а также с учетом статуса пользователя.
	*/
    public function index(Students $students, Upload $upload, Groups $groups, $pagination=1)
    {  
			# На странице отображается по 20 обнолвений, поскольку в размере они достаточно крупные.
    		$itemsPerPage = 20;
			$users = new User;
			$user = $users->getUser();
			/*
			 Определяем, что выборка формируется для преподавателя, у которого есть подконтрольные группы и привязка к структуре университета.
			*/
			if((Auth::user()->status == "teacher") and (Auth::user()->gruppa != "none") and (Auth::user()->teacher_groups != "none")){
				# Выбираем группы, находящиеся внутри кафедры, к которой привязан преподаватель.
				$grpByKaf = $groups->getGroupsByKaf(Auth::user()->gruppa);
				if(count($grpByKaf)>0){
					# Формируем массив групп внутри кафедры.
					foreach($grpByKaf as $grpid){
						$kafGrp[] = $grpid->id;
					}
					# Получаем список всех студентов, относящихся к той же кафедре, что и преподаватель.
					$allKafGrpStd = $students->getMassStudentsByGroup($kafGrp);
					# Получаем список подконтрольных преподавателю групп.
					$teacherGroups = $groups->getTeacherGroups(Auth::user()->teacher_groups);
					# Превращаем список подконтрольных групп из строки в массив.
					$tgroupsArr = explode(',',Auth::user()->teacher_groups);
					# Выбираем студентов из тех групп, которые являются подконтрольными данному преподавателю.
					$grpStudents = $students->getStudentsByKaf($tgroupsArr);
					# Получаем список загрузок студентов подконтрольных преподавателю групп.
					$showUploads = $upload->getUploadsByVisiblity($pagination, $itemsPerPage, $allKafGrpStd);
					# Получаем общее количество загрузок студентов подконтрольных преподавателю групп.
					$uploadsSummary = $upload->getUploadsCountByVisiblity($allKafGrpStd);
				}
			}elseif(Auth::user()->status == "monitor"){
			# Определяем, что выборка формируется для старосты.
				# Выбираем студентов из той же группы, что и староста.
				$grpStudents = $students->getStudentsByGroup(Auth::user()->gruppa);
				# Выбирем загрузки студентов из той же группы, что и староста.
				$showUploads = $upload->getUploadsByVisiblity($pagination, $itemsPerPage);
				# Считаем общее количество этих загрузок.
				$uploadsSummary = $upload->getUploadsCountByVisiblity();
			}else{
			# Выясняем, что пользователь не преподаватель и не староста, а значит -- студент.
				# Устанавливаем право доступа только к собственным загрузкам.
				$grpStudents = $students->getStudent(Auth::user()->id);
				# Выбираем собственные загрузки студента, а также иные общедоступные файлы других пользователей.
				$showUploads = $upload->getUploadsByVisiblity($pagination, $itemsPerPage);
				# Считаем суммарное количество загрузок, удовлетворяющих нашим требованиям.
				$uploadsSummary = $upload->getUploadsCountByVisiblity();
			}
			# Получаем список пользователей для отображения их имен и аватаров.
			$userNames = $students->getUsernames();
			# Считаем количество страниц на основании полученных данных.
			$pagesInSection = ceil($uploadsSummary/$itemsPerPage);
			return view('home')
				->with('grpStudents',$grpStudents)
				->with('users',$user)
				->with('showUploads',$showUploads)
				->with('uploadsSummary',$uploadsSummary)
				->with('userNames',$userNames)
				->with('itemsPerPage', $itemsPerPage)
				->with('pagination', $pagination)
				->with('pagesInSection', $pagesInSection);;
    }
}
