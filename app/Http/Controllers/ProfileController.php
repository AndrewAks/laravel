<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    # Коробочная функция, обеспечивающая работу авторизации.
    public function __construct()
    {
        $this->middleware('auth');
    }

    # Функция, обеспечивающая отображение представления profile (Насколько помню, нигде не применяется).
    public function index()
    {
        return view('profile');
    }
}
