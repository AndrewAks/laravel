<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Groups extends Model
{
	# Устанавливается, что в рамках данной модели будет использоваться таблица `groups`
    protected $table = 'groups';

	# Функция getGroups возвращает список групп, отсортиованный в алфавитном порядке.
    public function getGroups(){
        $group = DB::table('groups')->orderBy('name', 'asc')->get();
        return $group;
    }
	
	# Функция getGroup выводит информацию об отдельной группе, поиск просходит по идентификатору
    public function getGroup($id){
        $group = DB::table('groups')->where('id', $id)->first();
        return $group;
    }
	
	# Функция getGroupsByKaf выводит список групп в рамках одной каферды. Выборка происходит по идентификатору кафедры (kafedra_id)
    public function getGroupsByKaf($id){
        $group = DB::table('groups')->where('kafedra_id', $id)->get();
        return $group;
    }
	
	/*
	 Функция getTeacherGroups выводит информацию о группах, установленных в профиле преподавателя в качестве подотчетных.
	 В качестве входящих данных принимает список идентификаторов групп (строка), разделенных между собой запятыми. В последствии,
	 Список превращается в массив при помощи разбиения по запятой. Такой механизм был выбран как максимально удобный из-за того,
	 что в базе данных пользователей данные о подотчетных группах преподавателя хранятся именно в таком формате.
	*/
    public function getTeacherGroups($id){
    	  $grpSearch = explode(',', $id);
        $group = DB::table('groups')->whereIn('id', $grpSearch)->get();
        return $group;
    }

}
