<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class Upload extends Model
{
	# Настоящим устанавливается, что в рамках данной модели будет использоваться таблица `uploads`
    protected $table = 'uploads';

	#Функция create принимает массив данных $info, содержащий информацию о загружаемом файле или документе и обеспечивает регистрацию данных в таблице `uploads`
    public function create($info){
        DB::table('uploads')->where('id', Auth::user()->id)->insert([
			'title'=>$info['title'],
			'filename'=>$info['filename'],
			'type'=>$info['type'],
			'filetype'=>$info['filetype'],
            'created_at' => DB::raw ('CURRENT_TIMESTAMP'),
			'updated_at' => DB::raw ('CURRENT_TIMESTAMP'),
			'description'=>$info['description'],
			'owner_id'=>$info['owner_id'],
			'originating_owner'=>$info['originating_owner'],
			'viewable'=>$info['viewable'],
        ]);
        return;
    }
	
	# Функция getUploadsById обеспечивает вывод последних десяти загруженных файлов либо документов по одному конкретному пользователю, обозначенному идентификатором.
    public function getUploadsById($id){
        $uploads = DB::table('uploads')->where('owner_id', $id)->limit(10)->orderBy('created_at', 'desc')->get();
        return $uploads;
    }
	/*
	 Функция getUploadsByOriginatingId обеспечивает вывод десяти последних загруженных файлов либо документов по реальному (а не отображаемому, как getUploadsById) автору, обозначенному идентификатором.
	*/
    public function getUploadsByOriginatingId($id){
        $uploads = DB::table('uploads')->where('originating_owner', $id)->limit(10)->orderBy('created_at', 'desc')->get();
        return $uploads;
    }
	
	/*
	 Функция displayUploadsById обеспечивает вывод всех загруженных файлов и документов с сортировкой по дате размещения в сисеме в порядке убывания по одному конкретному пользователю, обозначенному идентификатором. Функция поддерживает постраничный вывод, поэтому кроме $id (идентификатор пользователя) и $type (тип запрашиваемых загрузок) принимает также номер просматриваемой страницы ($page) и количество вхождений на одну страницу вывода ($itemsPerPage), что позволяет обеспечивать постраничный вывод данных.
	*/
    public function displayUploadsById($id, $type, $page, $itemsPerPage){
    	  if($page == 1){
    	  		$begin = 0;
    	  }else{
				$begin = ($page-1)*($itemsPerPage);
    	  }
		  # $type == any -- это загрузки всех типов одновременно (документы, файлы, видео).
    	  if($type == "any"){
	        $uploads = DB::table('uploads')->where('owner_id', $id)->offset($begin)->limit($itemsPerPage)->orderBy('created_at', 'desc')->get();
	     }else{
	        $uploads = DB::table('uploads')->where('owner_id', $id)->where('type', $type)->offset($begin)->limit($itemsPerPage)->orderBy('created_at', 'desc')->get();
	     }
        return $uploads;
    }
	
	/*
	 Функция displayUploadsByOriginatingId осуществляет вывод списка загруженных документов либо файлов пользователя (поддерживается выбор типа загрузок) по общему признаку -- идентификатору реального автора (а не по идентификатору отображаемого, как displayUploadsById) с постраничным разбиением и сортировкой по дате добавления в порядке убывания.
	*/
    public function displayUploadsByOriginatingId($id, $type, $page, $itemsPerPage){
    	  if($page == 1){
    	  		$begin = 0;
    	  }else{
				$begin = ($page-1)*($itemsPerPage);
    	  }
		  # $type == any -- это загрузки всех типов одновременно (документы, файлы, видео).
    	  if($type == "any"){
	        $uploads = DB::table('uploads')->where('originating_owner', $id)->offset($begin)->limit($itemsPerPage)->orderBy('created_at', 'desc')->get();
	     }else{
	        $uploads = DB::table('uploads')->where('originating_owner', $id)->where('type', $type)->offset($begin)->limit($itemsPerPage)->orderBy('created_at', 'desc')->get();
	     }
        return $uploads;
    }
	
	/*
	 Функция getUploadsByVisiblity обеспечивает вывод загрузок пользователей с учетом указанных прав доступа по части области видимости. Данной функцией определяется статус пользователя в системе, на основании чего запрос на выборку данных меняется, поскольку для разных статусов предусмотрены различные уровни доступа. Сортировка данных в выводе происходит по дате размещения документа в системе в порядке убывания, также функцией поддерживается постраничный вывод. Используется в ленте загрузок.
	*/
    public function getUploadsByVisiblity($page, $itemsPerPage, $groups='null'){
		if($page == 1){
			$begin = 0;
		}else{
			$begin = ($page-1)*($itemsPerPage);
		}
		# Если пользователь имеет статус преподавателя...
		if(Auth::user()->status == "teacher"){
			/*
			 ...то ему доступны документы: 
				- с видимостью "для всех" (overall), 
				- с видимостью "только для меня" (self) при условии, что это его загрузки, 
				- с видимостью "только для моей группы" (group) при условии, что файлы были загружены пользователем, который относится к подконтрольной преподавателю группе.
			*/
			$uploads = DB::select(DB::raw("SELECT * FROM `uploads` WHERE ((`viewable`='overall') OR ((`viewable`='self') AND (`originating_owner`=:ownerId)) OR ((`viewable`='group') AND (`owner_id` IN (SELECT `id` FROM `users` WHERE `gruppa`=:group AND `status`!='teacher')))) ORDER BY `created_at` DESC LIMIT :begin,:end;"), array('ownerId' => Auth::user()->id, 'group' => Auth::user()->gruppa, 'begin' => $begin, 'end' => $itemsPerPage));
		}else{
			/*
			 А если пользователь не преподаватель, то он видит:
				- собственные загрузки (self),
				- общедоступные загрузки (overall),
				- загрузки членов той же группы, в которой числится и сам пользователь, при условии, что они имеют область видимости "для всей группы" (group).
			*/
	    	$uploads = DB::select(DB::raw("SELECT * FROM `uploads` WHERE ((`viewable`='overall') OR ((`viewable`='self') AND (`originating_owner`=:ownerId)) OR ((`viewable`='group') AND (`owner_id` IN (SELECT `id` FROM `users` WHERE `gruppa`=:group AND `status`!='teacher')))) ORDER BY `created_at` DESC LIMIT :begin,:end;"), array('ownerId' => Auth::user()->id, 'group' => Auth::user()->gruppa, 'begin' => $begin, 'end' => $itemsPerPage));
	   }
    	return $uploads;
    }

	# Функция getUploadsCountByVisiblity является дополнением к функции getUploadsByVisiblity и возвращает количество доступных к показу загруженных файлов на тех же условиях.
    public function getUploadsCountByVisiblity($groups='null'){
		if(Auth::user()->status == "teacher"){
			# То же самое примечание, что и в стр. 92-97, только с поправкой на то, что там был вывод данных, а здесь они просто считаются в количество подходящих для отображения вхождений.
			$uploads = DB::select(DB::raw("SELECT COUNT(0) FROM `uploads` WHERE ((`viewable`='overall') OR ((`viewable`='self') AND (`originating_owner`=:ownerId)) OR ((`viewable`='group') AND (`owner_id` IN (SELECT `id` FROM `users` WHERE `gruppa`=:group AND `status`!='teacher'))));"), array('ownerId' => Auth::user()->id, 'group' => Auth::user()->gruppa));
		}else{
			# То же самое примечание, что и в стр. 100-105, учитывая стр. 114
	    	$uploads = DB::select(DB::raw("SELECT COUNT(0) FROM `uploads` WHERE ((`viewable`='overall') OR ((`viewable`='self') AND (`originating_owner`=:ownerId)) OR ((`viewable`='group') AND (`owner_id` IN (SELECT `id` FROM `users` WHERE `gruppa`=:group AND `status`!='teacher'))));"), array('ownerId' => Auth::user()->id, 'group' => Auth::user()->gruppa));
	   }
	   foreach($uploads[0] as $value){
	   	return $value;
	   	break;
	   }
    }
	
	# Функция summaryCount выводит число, обозначающее количество файлов в системе, которые связаны с пользователем, идентификатор учетной записи которого передается через переменную $id.
    public function summaryCount($status, $id, $type){
    	  if($status == "teacher"){
			# Для преподавателей поиск происходит по реальному авторству
	        $uploads = DB::table('uploads')->where('originating_owner', $id)->count();
	     }else{
			# Для обычных пользователей по отображаемому.
	     	$uploads = DB::table('uploads')->where('owner_id', $id)->count();
	     }
        return $uploads;
    }
	
	/*
	 Функция getFile возвращает информацию о конкретной строке из таблицы `uploads`, содержащей информацию о загруженном файле либо документе и его аторах (реальном и отображаемом), с поиском по имени файла.
	*/
    public function getFile($id){
        $uploads = DB::table('uploads')->where('filename', $id)->first();
        return $uploads;
    }
	
	# Функция removeFile удаляет из таблицы `uploads` вхождение о том или ином загруженном документе либо файле.
    public function removeFile($id){
        $uploads = DB::table('uploads')->where('id', $id)->delete();
        return $uploads;
    }
	
	/*
	 Функция getFile возвращает информацию о конкретной строке из таблицы `uploads`, содержащей информацию о загруженном файле либо документе и его аторах (реальном и отображаемом), с поиском по идентификатору вхождения в таблице `uploads`.
	*/
    public function getFileById($id){
        $uploads = DB::table('uploads')->where('id', $id)->first();
        return $uploads;
    }
}
