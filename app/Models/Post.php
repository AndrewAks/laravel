<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class Post extends Model
{
	# Задается, что в рамках данной модели будет использоваться таблица `posts`
    protected $table = 'posts';

	/*
	 Функция getPost выводит список записей из таблицы `posts` с сортировкой по идентификатору. В случае, если функции была передана переменная $paginate, осуществляется также постраничное разбиение вывода.
	*/
    public function getPost($paginate){
        if($paginate == 0){
            $post = DB::table('posts')->orderBy('id', 'desc')->get();
        }else{
            $post = DB::table('posts')->orderBy('id', 'desc')->paginate($paginate);
        }
        unset($paginate);
        return $post;
    }

	/*
	 Функция create осуществляет обновление значения поля `gruppa` в таблице `users` в случае, если идентификатор авторизованного пользователя, производящего настоящее действие, равен переданному в массиве $info
	*/
    public function create($info){
        $groupId = $info['gruppa'];
        if(Auth::user()->id == $info['user_id']){
             DB::table('users')->where('id', Auth::user()->id)->update([
                 'gruppa'=>$groupId,
                 'updated_at' => DB::raw ('CURRENT_TIMESTAMP'),
             ]);
         }else{
	    	 #access violation
	 		}
         return;
    }
    
	/*
	 Функция ifInTeacherGroups осуществляет проверку, входит ли передаваемый данной функции идентификатор группы $id в список подконтрольных групп преподавателя, от имени которого функция в данный конкретный момент исполняется. Также установлена проверка, является ли авторизованный пользователь преподавателем.
	*/
	 public function ifInTeacherGroups($id){
        $tgrp = DB::table('users')->where('id', Auth::user()->id)->first();
        if($tgrp->teacher_groups == "none"){
        		$return = 'false';
        }else{
	        $tgroups = explode(',', $tgrp->teacher_groups);
	        if(in_array($id,$tgroups)){
	        		$return = 'true';
	        }else{
	        		$return = 'false';
	        }
	     }
        return $return;
    }
}
