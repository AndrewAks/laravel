<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Students extends Model
{
	/*
	 Указывает, что в рамках данной модели будет использоваться таблица `users`. В нашем проекте, список студентов, старост и преподавателей для большей гибкости и простоты обращения объединены со списком пользователей.
	*/
    protected $table = 'users';

	# Функция getStudents возвращает список пользователей из таблицы `users` с сортировкой по идентификатору в порядке возрастания
    public function getStudents(){
        $student = DB::table('users')->orderBy('id', 'asc')->get();
        return $student;
    }
	
	# Функция getStudent осуществляет вывод данных о конкретном пользователе по переданному ей идентификатору.
    public function getStudent($id){
        $student = DB::table('users')->where('id', $id)->get();
        return $student;
    }
	
	/* 
	 Функция getStudentsByGroup осуществляет вывод списка студентов, объединенных общим значением -- идентификатором группы. Таким образом, данная функция позволит вывести всех студентов из одной группы в список, отсортированный в порядке возрастания по идентификатору.
	*/
	 public function getStudentsByGroup($group){
        $student = DB::table('users')->where('gruppa', $group)->orderBy('id', 'asc')->get();
        return $student;
    }
	
	/*
	 Функция getMassStudentsByGroup осуществляет вывод только студентов и старост (преподаватели в данном выводе не будут отображаться) по общему признаку -- идентификатору группы. Однако, в отличие от предшествующей функции, данная работает не с отдельным значением, а со списком групп, что позволяет осуществлять получение данных о студентах одновременно нескольких групп с объединением в общий список.
	*/
    public function getMassStudentsByGroup($group){
        $student = DB::table('users')->whereIn('gruppa', $group)->where('status', '!=', 'teacher')->orderBy('id', 'asc')->get();
        return $student;
    }
	
	/*
	 Функция getStudentsByKaf обеспечивает возвращение данных о пользователях по общему признаку -- идентификатору группы. Вывод данной функции используется при формировании списка пользователей, в аккаунты которых другой пользователь может осуществлять загрузку файлов и документов, поэтому для преподавателей добавлено отдельное правило, позволяющее добавить к списку также и аккаунт того преподавателя, для которого формируется список. В конечном итоге, это позволяет преподавателю загружать файлы и документы также и в собственный аккаунт.
	*/
	 public function getStudentsByKaf($groups){
		if(Auth::user()->status == "teacher"){
			$student = DB::table('users')->whereIn('gruppa', $groups)->orWhere('id', Auth::user()->id)->orderBy('id', 'asc')->get();
		}else{
			$student = DB::table('users')->whereIn('gruppa', $groups)->orderBy('id', 'asc')->get();
		}
			return $student;
    }
	
	/*
	 Функция getUsernames возвращает ассоциативный массив информации о пользователях, позволяющий по идентификатору пользователя установить его имя и получить доступ к адресу загруженного пользователем аватара. Используется для отображения информации о пользователях в ленте документов.
	*/
    public function getUsernames(){
        $users = DB::table('users')->get();
        foreach($users as $user){
        		$username[$user->id] = $user->name;
        		$username[$user->id.'_avatar'] = $user->avatar;
        }
        return $username;
    }
	
	/*
	 Функция getUnverified позволяет вывести список пользователей, отправивших заявки на создание учетных записей, но еще не подтвержденных администратором ресурса. Поскольку данной функцией поддерживается вывод информации в постраничном режиме, то переменные $page и $itemsPerPage обеспечивают, соответственно, передачу в функцию информации о просматриваемой в данный момент странице и о количестве вхождений на страницу, которые должны быть возвращены в ответ.
	*/
    public function getUnverified($page, $itemsPerPage){
			# Если пользователь на первой странице, то мы начинаем выводить записи с самого начала (то есть, с нулевого идентификатора)
			if($page == 1){
				$begin = 0;
			}else{
				# Иначе же все происходит по формуле, согласно которой номер первой отображаемой записи равен (Номеру страницы минус один) и умноженному на количество вхождений на страницу
				$begin = ($page-1)*($itemsPerPage);
			}
			$users = DB::table('users')->where('verified', '!=', 1)->offset($begin)->limit($itemsPerPage)->get();
			return $users;
    }
	
	# Функция getUnverifiedSum возвращает количество пользователей, аккаунты которых не были верифицированы администратором ресурса. Используется в постраничной навигации.
    public function getUnverifiedSum(){
        $users = DB::table('users')->where('verified', '!=', 1)->count();
        return $users;
    }
	
	# Функция declineUser реализует отказ в верификации аккаунта пользователя, имеющего идентификатор $id. Вызывается из панели верификации регистрации преподавателей и старост.
    public function declineUser($id){
        $users = DB::table('users')->where('id', $id)->delete();
        return $users;
    }
	
	# Функция verifyUser реализует подтверждение (верификацию) регистрации преподалателя либо старосты. Вызывается из панели верификации регистраций преподавателей и старост.
    public function verifyUser($id){
        $users = DB::table('users')->where('id', $id)->update(array('verified' => 1));
        return $users;
    }
}
