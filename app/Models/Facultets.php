<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Facultets extends Model
{
	# Устанавливается, что в рамках данной модели будет использоваться таблица `facultets`
    protected $table = 'facultets';

	# Функция getFacultets вызвращает при обращении к себе список факультетов, отсортированных по идентификатору в обратном порядке (зачем? это можно убрать)
    public function getFacultets(){
        $facultet = DB::table('facultets')->orderBy('id', 'desc')->get();
        return $facultet;
    }

	# Фукнция getFacultet возвращает строку из таблицы `facultets`, где идентификатор равен запрашиваемому. Используется для получения данных о конкретном факультете.
    public function getFacultet($id){
		$facultet = DB::table('facultets')->where('id', $id)->first();
		return $facultet;
    }

}
