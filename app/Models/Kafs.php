<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kafs extends Model
{
	# Устанавливает, что в рамках данной модели будет использоваться таблица `kafs`
    protected $table = 'kafs';

	# Функция getKafs возвращает список кафедр, зарегитрированных в системе, с сортиовкой по названию в алфавитном порядке.
    public function getKafs(){
        $kaf = DB::table('kafs')->orderBy('name', 'asc')->get();
        return $kaf;
    }
	
	# Функция getKaf выводит информацию о конкретной кафедре из базы данных, выбирая ее по идентификатору.
    public function getKaf($id){
        $kaf = DB::table('kafs')->where('id', $id)->first();
        return $kaf;
    }

}
