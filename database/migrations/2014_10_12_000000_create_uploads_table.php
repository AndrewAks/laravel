<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('filename')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
			$table->text('description');
			$table->string('filetype')->nullable();
            $table->integer('owner_id')->unsigned();
			$table->integer('originating_owner')->unsigned();
			$table->string('viewable')->default('self');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uploads');
    }
}
