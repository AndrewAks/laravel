<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
			 	$table->string('status');
			 	$table->string('gruppa')->default('none');
			 	$table->string('teacher_groups')->default('none');
            $table->string('password');
            $table->string('reg_ip');
            $table->integer('verified')->default(0);
            $table->integer('is_admin')->default(0);
            $table->string('avatar')->default('none');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
