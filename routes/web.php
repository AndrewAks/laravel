<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Post;


Auth::routes();

Route::get('/','HomeController@index')->name('home');
Route::get('/page-{pagination}','HomeController@index');
Route::get('/my','MyController@index')->name('my');
Route::get('/my/page-{pagination}','MyController@index');
Route::get('/my/{doctype}s','MyController@index');
Route::get('/my/{doctype}s/page-{pagination}','MyController@index');
Route::get('/download/{fileid}', 'DownloadController@index');
Route::get('/profile', 'MainController@index')->name('profile');
Route::get('/remove/{identifier}', 'MainController@remove');
Route::get('/users/verify', 'VerifyController@index');
Route::get('/users/verify/page-{pagination}', 'VerifyController@index');
Route::get('/users/verify/{user_id}/accept', 'VerifyController@accept');
Route::get('/users/verify/{user_id}/decline', 'VerifyController@decline');
Route::get('/my/export', 'MyController@export');
Route::post('/profile/set', 'MainController@setGroup')->name('post');
Route::post('profile', 'MainController@update_avatar');
Route::post('/loadDocument', 'MainController@loadDocument');
Route::post('/setTeacherGroups', 'MainController@setTeacherGroups');
