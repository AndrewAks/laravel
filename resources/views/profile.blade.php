@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Профиль пользователя
                	@if(Auth::user()->is_admin == 1)
                		<input type="button" class="pull-right btn btn-sm btn-primary" value="Верификация" onClick="javascript:document.location.href='/users/verify';" style="margin-left:5px;">
                	@endif
					<a href="/my/export"><input type="button" class="pull-right btn btn-sm btn-primary" value="Экспорт профиля" style="margin-left:5px;"></a>
                	<input type="button" class="pull-right btn btn-sm btn-primary" value="Изменить фотографию профиля" data-toggle="modal" data-target="#myModal">
                </div>
                <div class="panel-body">
                	<table style="width:100%;">
                		<tr>
                			<td style="width:200px;text-align:center;">
                @if (Auth::user()->avatar != "none")
		                    <img src="/img/avatars/{{ Auth::user()->avatar }}" style="width:150px; height: 150px;"><br>
                @else
   		                 <img src="/img/avatars/male-avatar.png" style="width:150px; height: 150px;"><br>
                @endif
                			</td>
                			<td style="text-align:left;">
		                    <h4> ФИО: {{ Auth::user()->name }} </h4>
   		                 <h4> Е-mail для связи: {{ Auth::user()->email }} </h4>
      		              <h4> Дата регистрации: {{ Auth::user()->created_at }} </h4>
		   					  <h4> Статус пользователя: 
									 @if(Auth::user()->status == "teacher")
										Преподаватель
									 @elseif(Auth::user()->status == "monitor")
										Староста
									 @elseif(Auth::user()->status == "student")
										Студент
									 @endif
		    					  </h4>
		    					  @if(Auth::user()->verified != 1)
		    					  <h4> Статус регистрации: Происходит верификация </h4>
		    					  @endif
                			</td>
                		</tr>
                	</table>
		@if(Auth::user()->gruppa == "none")
		    <h4> Выбор места в структуре университета: </h4>
		    <div align="center">
		    <form method="POST" action="{{ url('/profile/set') }}">
			<select name="gruppa" style="width:75%;">
				@if(Auth::user()->status == "teacher")
	            @foreach($facultet as $facultets)
        			<optgroup label="{{$facultets->name}}">
                	    @foreach($kaf as $kafs)
                       		@if($facultets->id == $kafs->facultet_id)
        				<option value="{{$kafs->id}}">{{$kafs->name}}</option>
									@endif
        		   		 @endforeach
        			</optgroup>
				   @endforeach
        		@else
	            @foreach($facultet as $facultets)
        			<optgroup label="{{$facultets->name}}">
                	    @foreach($kaf as $kafs)
                       		@if($facultets->id == $kafs->facultet_id)
        				<optgroup label="->   {{$kafs->name}}">
										@foreach($group as $groups)
											@if($kafs->id == $groups->kafedra_id)
										<option value="{{$groups->id}}">{{$groups->name}}</option>
											@endif
										@endforeach
						</optgroup>
									@endif
        		   		 @endforeach
        			</optgroup>
				   @endforeach
        		@endif
			</select>
			<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="submit" class="btn btn-sm btn-primary" value="Выбрать">
		    </form>
		@else
                    <h4 style="padding-bottom:10px;"> Место в структуре университета:<br><br>
							-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$facultet->name}}<br>
							&nbsp;&nbsp;-->&nbsp;&nbsp;&nbsp; {{$kaf->name}}
		    @if(Auth::user()->status != "teacher")
						<br>&nbsp;&nbsp;&nbsp;&nbsp;-->&nbsp;&nbsp;Группа {{$group->name}}
		    @endif
		    </h4>
		@endif
		@if((Auth::user()->status == "teacher") and (Auth::user()->gruppa != "none"))
			  <h4><img src="/img/edit.png" alt="" data-toggle="modal" data-target="#changeGroups" style="cursor:pointer;">Курируемые группы в рамках кафедры:</h4>
			@if(Auth::user()->teacher_groups == "none")
				<form action="/setTeacherGroups" method="POST">
					<select name="teacherGroups[]" style="width:75%;" multiple>
						@if(count($groupByKaf)>0)
							@foreach($groupByKaf as $grpBkaf)
							<option value="{{$grpBkaf->id}}">{{$grpBkaf->name}}</option>
							@endforeach
						@else
							<option disabled>На данном факультете группы отсутствуют.</option>
						@endif
					</select>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="submit" class="pull-right btn btn-sm btn-primary" value="Сохранить">
				</form>
			@elseif(Auth::user()->teacher_groups != "none")
				@foreach($teacherGroups as $tgrp)
					&bull; {{ $tgrp->name }} <br>
				@endforeach
			@endif
		@endif
		@if(Auth::user()->verified == 1)
							<hr>
                    <h4> Последние загруженные документы: <input type="button" class="pull-right btn btn-sm btn-primary" value="Загрузить документ" data-toggle="modal" data-target="#file_load" style="display:inline-block;"></h4>
			@if($postCount>0)
			<div style="height:5px;"></div>
			<script type="text/javascript">
				function confirmRemove(id){
					if (confirm("Вы уверены, что желаете удалить данный файл? Предупреждений больше не будет.")) {
					  window.location.href = '/remove/' + id;
					} else {
					  alert("Не хотите, как хотите.")
					}
				}
			</script>
			<table style="width:100%;">
			@foreach($scanUploads as $thisUpload)
				<tr style="background-color:#f1f1f1;">
			@if(($thisUpload->type == "video") or ($thisUpload->type == "file"))
				<td style="width:45px;" align="center"><img src="/img/{{ $thisUpload->filetype }}.png" alt="{{ $thisUpload->title }}"></td>
			@else
				<td style="width:45px;"><img src="/uploads/{{ $thisUpload->filename }}.{{ $thisUpload->filetype }}" style="width:40px;" alt="{{ $thisUpload->title }}"></td>
			@endif
					<td>
						<strong>
			@if($thisUpload->type == "video")
							<a href="/uploads/{{ $thisUpload->filename }}.{{ $thisUpload->filetype }}" target="_blank">{{ $thisUpload->title }}</a>
			@elseif($thisUpload->type == "file")
							<a href="/download/{{ $thisUpload->filename }}" target="_blank">{{ $thisUpload->title }}</a>
			@else
							<a href="/uploads/{{ $thisUpload->filename }}_full.{{ $thisUpload->filetype }}" target="_blank">{{ $thisUpload->title }}</a>
			@endif
						</strong><br>
						{{ $thisUpload->description }}
					</td>
					<td>{{ $thisUpload->created_at }}</td>
				@if($thisUpload->originating_owner == $thisUpload->owner_id)
					<td><span title="Это Вы">{{ $userNames[$thisUpload->originating_owner] }}</span></td>
				@else
					<td><span title="Это Вы">{{ $userNames[$thisUpload->originating_owner] }}</span> для {{ $userNames[$thisUpload->owner_id] }}</td>
				@endif
					<td style="width:20px;text-align:center;" align="center" valign="middle"><img src="/img/remove.png" alt="" onClick="javascript:confirmRemove('{{ $thisUpload->id }}');"></td>
				</tr>
				<tr style="height:3px;"><td colspan="5" style="height:3px;"></td></tr>
			@endforeach
			</table>
			@else
				 В настоящий момент публикации отсутствуют.
			@endif
		@endif
			@if((Auth::user()->status == "teacher") and (Auth::user()->gruppa != "none"))
					<div id="changeGroups" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<form action="/setTeacherGroups" method="POST">
									<div class="modal-header">
										<button class="close" type="button" data-dismiss="modal">×</button>
										<h4 class="modal-title">Изменение списка курируемых групп</h4>
									</div>
									<div class="modal-body">
										<select name="teacherGroups[]" style="width:100%;" multiple>
											@if(count($groupByKaf)>0)
												@foreach($groupByKaf as $grpBkaf)
													@if($slaveGroups[$grpBkaf->id] == "true")
													<option value="{{$grpBkaf->id}}" selected>{{$grpBkaf->name}}</option>
													@else
													<option value="{{$grpBkaf->id}}">{{$grpBkaf->name}}</option>
													@endif
												@endforeach
											@else
												<option disabled>На данном факультете гуппы отсутствуют.</option>
											@endif
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									<div class="modal-footer">
										<input type="submit" class="pull-left btn btn-sm btn-primary" value="Сохранить">
										<button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
									</div>
								</form>
							</div>
						</div>
					</div>
			@endif
					<div id="myModal" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<form enctype="multipart/form-data" action="/profile" method="POST">
									<div class="modal-header">
										<button class="close" type="button" data-dismiss="modal">×</button>
										<h4 class="modal-title">Изменение фотографии профиля</h4>
									</div>
									<div class="modal-body">
										<input type="file" name="avatar" required>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									<div class="modal-footer">
										<input type="submit" class="pull-left btn btn-sm btn-primary" value="Загрузить">
										<button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@if(Auth::user()->verified == 1)
					<div id="file_load" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<form enctype="multipart/form-data" action="/loadDocument" method="POST">
									<div class="modal-header">
										<button class="close" type="button" data-dismiss="modal">×</button>
										<h4 class="modal-title">Загрузка документа</h4>
									</div>
									<div class="modal-body">
										<input type="text" name="title" style="width:100%;margin-bottom:10px;" placeholder="Наименование документа" required>
										<table style="width:100%;">
											<tr>
												<td style="width:50%;"><input type="file" name="document" required></td>
												<td style="width:50%;">
													<select name="doctype" style="width:100%;">
														<optgroup label="Выберите тип создаваемого документа">
															<option value="image">Графический документ</option>
															<option value="file">Файл</option>
															<option value="video">Видеозапись</option>
														</optgroup>
													</select>
												</td>
											</tr>
										</table>
										<textarea name="description" style="width:100%;height:140px;margin-top:10px;" required></textarea>
										<table style="width:100%;">
											<tr>
												<td style="width:50%;" align="left">
													Владелец:
													<select name="owner_id">
													@foreach($students as $student)
														<option value="{{$student->id}}" selected>{{$student->name}}</option>
													@endforeach
													</select>
												</td>
												<td style="width:50%;" align="right">
													<select name="viewable">
														<optgroup label="Область видимости">
															<option value="self" selected>Только автор</option>
															<option value="group">Вся группа</option>
															<option value="overall">Все пользователи</option>
														</optgroup>
													</select>
												</td>
											</tr>
										</table>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									<div class="modal-footer">
										<input type="submit" class="pull-left btn btn-sm btn-primary" value="Загрузить">
										<button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
