@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h3 class="col-md-3 News">Мои документы</h3>
                        <input type="button" style="margin-right:5px;" class="pull-right btn btn-sm btn-primary" value="Файлы" onClick="javascript:gotoSection('files');">
                        <input type="button" style="margin-right:5px;" class="pull-right btn btn-sm btn-primary" value="Видео" onClick="javascript:gotoSection('videos');">
                        <input type="button" style="margin-right:5px;" class="pull-right btn btn-sm btn-primary" value="Изображения" onClick="javascript:gotoSection('images');">
                        <input type="button" style="margin-right:5px;" class="pull-right btn btn-sm btn-primary" value="Все" onClick="javascript:gotoSection('any');">
                    </div><!-- /.row -->
                </div>
                <div class="panel-body">
                @if(Auth::user()->verified == 1)
						<script type="text/javascript">
							function confirmRemove(id){
								if (confirm("Вы уверены, что желаете удалить данный файл? Предупреждений больше не будет.")) {
								  window.location.href = '/remove/' + id;
								} else {
								  alert("Не хотите, как хотите.")
								}
							}
							function pagination(direction){
								var currentURL1 = '/my';
								var docType = '{{ $rqDocType }}';
								if(docType != "any"){
									var currentURL2 = currentURL1 + '/' + docType + 's';
								}else{
									var currentURL2 = currentURL1;
								}
								var currentPage = {{ $pagination }};
								if(direction == "back"){
									if(currentPage>2){
										var currentURL3 = currentURL2 + '/page-' + (currentPage-1);
									}else if(currentPage == 2){
										var currentURL3 = currentURL2;
									}
								}else if(direction == "forward"){
									var currentURL3 = currentURL2 + '/page-' + (currentPage+1);
								}
								window.location.href = currentURL3;
							}
							function gotoSection(section){
								var currentURL1 = '/my';
								if(section == "any"){
									var currentURL2 = currentURL1;
								}else{
									var currentURL2 = currentURL1 + '/' + section;
								}
								window.location.href = currentURL2;
							}
						</script>
						@if($postCount>0)
						<div style="height:5px;"></div>
						<table style="width:100%;">
							@foreach($scanUploads as $thisUpload)
							<tr style="background-color:#f1f1f1;">
								@if(($thisUpload->type == "video") or ($thisUpload->type == "file"))
								<td style="width:45px;" align="center"><img src="/img/{{ $thisUpload->filetype }}.png" alt="{{ $thisUpload->title }}"></td>
								@else
								<td style="width:45px;"><img src="/uploads/{{ $thisUpload->filename }}.{{ $thisUpload->filetype }}" style="width:40px;" alt="{{ $thisUpload->title }}"></td>
								@endif
								<td>
									<strong>
								@if($thisUpload->type == "video")
										<a href="/uploads/{{ $thisUpload->filename }}.{{ $thisUpload->filetype }}" target="_blank">{{ $thisUpload->title }}</a>
								@elseif($thisUpload->type == "file")
										<a href="/download/{{ $thisUpload->filename }}" target="_blank">{{ $thisUpload->title }}</a>
								@else
										<a href="/uploads/{{ $thisUpload->filename }}_full.{{ $thisUpload->filetype }}" target="_blank">{{ $thisUpload->title }}</a>
								@endif
									</strong><br>
									{{ $thisUpload->description }}
								</td>
								<td>{{ $thisUpload->created_at }}</td>
								@if($thisUpload->originating_owner == $thisUpload->owner_id)
								<td><span title="Это Вы">{{ $userNames[$thisUpload->originating_owner] }}</span></td>
								@else
								<td><span title="Это Вы">{{ $userNames[$thisUpload->originating_owner] }}</span> для {{ $userNames[$thisUpload->owner_id] }}</td>
								@endif
								<td style="width:20px;text-align:center;" align="center" valign="middle"><img src="/img/remove.png" alt="" onClick="javascript:confirmRemove('{{ $thisUpload->id }}');"></td>
							</tr>
							<tr style="height:3px;"><td colspan="5" style="height:3px;"></td></tr>
							@endforeach
						</table>
						@else
						 В настоящий момент документы по заданному критерию отсутствуют.
						@endif
						@if($summaryCount>$itemsPerPage)
						<br>
							@if($pagination > 1)
						<input type="button" class="pull-left btn btn-sm btn-primary" value="« Предыдущие" onClick="javascript:pagination('back');">
							@endif
							@if($pagination < $pagesInSection)
						<input type="button" class="pull-right btn btn-sm btn-primary" value="Следующие »" onClick="javascript:pagination('forward');">
							@endif
						@endif
					@else
					 	В настоящий момент Ваша регистрация еще не была подтверждена администратором системы. Пожалуйста, повторите обращение несколько позже. Заявки на верификацию обрабатываются в ручном режиме. До завершения процесса верификации полный функционал системы будет недоступен.
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
