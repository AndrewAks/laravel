@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Верификация
                </div>
                <div class="panel-body">
					   <script type="text/javascript">
							function confirmDecline(id){
								if (confirm("Вы уверены, что желаете отклонить данную регистрацию? Предупреждений больше не будет.")) {
								  window.location.href = '/users/verify/' + id + '/decline';
								} else {
								  alert("Не хотите, как хотите.")
								}
							}
							function confirmAccept(id){
								if (confirm("Вы уверены, что желаете одобрить данную регистрацию? Предупреждений больше не будет.")) {
								  window.location.href = '/users/verify/' + id + '/accept';
								} else {
								  alert("Не хотите, как хотите.")
								}
							}
							function pagination(direction){
								var currentURL1 = '/users/verify';
								var currentPage = {{ $pagination }};
								if(direction == "back"){
									if(currentPage>2){
										var currentURL2 = currentURL1 + '/page-' + (currentPage-1);
									}else if(currentPage == 2){
										var currentURL2 = currentURL1;
									}
								}else if(direction == "forward"){
									var currentURL2 = currentURL1 + '/page-' + (currentPage+1);
								}
								window.location.href = currentURL2;
							}
						</script>
						@if($unverifiedQ>0)
						<table style="width:100%;">
							<tr style="background-color:#f1f1f1;">
								<td style="font-weight:bold;">Имя пользователя</td>
								<td style="font-weight:bold;">Статус</td>
								<td style="font-weight:bold;">Почта</td>
								<td style="font-weight:bold;">Дата регистрации</td>
								<td style="font-weight:bold;">IP-Адрес</td>
								<td style="font-weight:bold;">Факультет/Группа</td>
								<td style="font-weight:bold;">Действие</td>
							</tr>
							<tr style="height:3px;"><td colspan="5" style="height:3px;"></td></tr>
							@foreach($unverified as $usrVer)
							<tr style="background-color:#f1f1f1;">
								<td>{{ $usrVer->name }}</td>
								<td>
								@if($usrVer->status == "teacher")
									Преподаватель
								@elseif($usrVer->status == "monitor")
									Староста
								@endif
								</td>
								<td>{{ $usrVer->email }}</td>
								<td>{{ $usrVer->created_at }}</td>
								<td>{{ $usrVer->reg_ip }}</td>
								<td>
								@if($usrVer->gruppa != "none")
									{{ $usrVer->gruppa }}
								@else
									Не выбран(а)
								@endif
								</td>
								<td align="center">
									<img src="/img/verify.png" alt="" onClick="javascript:confirmAccept('{{ $usrVer->id }}');" title="Одобрить" style="cursor:pointer;">
									<img src="/img/remove.png" alt="" onClick="javascript:confirmDecline('{{ $usrVer->id }}');" title="Отклонить" style="cursor:pointer;">
								</td>
							</tr>
							<tr style="height:3px;"><td colspan="5" style="height:3px;"></td></tr>
							@endforeach
						</table>
							@if($unverifiedSum>$itemsPerPage)
								<br>
								@if($pagination > 1)
								<input type="button" class="pull-left btn btn-sm btn-primary" value="« Предыдущие" onClick="javascript:pagination('back');">
								@endif
								@if($pagination < $pagesInSection)
								<input type="button" class="pull-right btn btn-sm btn-primary" value="Следующие »" onClick="javascript:pagination('forward');">
								@endif
							@endif
						@else
							 В настоящий момент запросы на верификацию отсутствуют.
						@endif
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
