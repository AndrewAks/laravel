@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h3 class="col-md-3 News">Новости</h3>
                    </div><!-- /.row -->
                </div>
                <div class="panel-body">
                	@if(Auth::user()->verified == 1)
							<script type="text/javascript">
								function pagination(direction){
									var currentURL1 = '/';
									var currentPage = {{ $pagination }};
									if(direction == "back"){
										if(currentPage>2){
											var currentURL2 = currentURL1 + 'page-' + (currentPage-1);
										}else if(currentPage == 2){
											var currentURL2 = currentURL1;
										}
									}else if(direction == "forward"){
										var currentURL2 = currentURL1 + 'page-' + (currentPage+1);
									}
									window.location.href = currentURL2;
								}

							</script>
                		@foreach($showUploads as $thisUpl)
								<div style="background:url('/img/clip.png') bottom right no-repeat #eaeaea;width:100%;margin-bottom:10px;padding-top:25px;padding-bottom:25px;padding-left:20px;">
									@if($userNames[$thisUpl->owner_id.'_avatar'] != "none")
										<img src="/img/avatars/{{ $userNames[$thisUpl->owner_id.'_avatar'] }}" style="width:32px;height:32px;border-radius:50%;" alt="{{ $userNames[$thisUpl->owner_id] }}">
									@else
										<img src="/img/avatars/male-avatar.png" style="width:32px;height:32px;border-radius:50%;" alt="{{ $userNames[$thisUpl->owner_id] }}">
									@endif
									@if($thisUpl->type == "image")
										Загружен новый графический документ 
									@elseif($thisUpl->type == "file")
										Загружен новый файл 
									@elseif($thisUpl->type == "video")
										Загружена новая видеозапись 
									@endif
									пользователем <strong>{{ $userNames[$thisUpl->owner_id] }}</strong>:
									<div style="background-image:url('/img/semi_transparent.png');background-repeat:repeat;width:auto;border-radius:10px;margin-top:10px;margin-right:25px;">
										<table style="width:100%;">
											<tr style="min-height:120px;">
												<td style="width:120px;text-align:center;vertical-align:middle;" align="center">
													@if($thisUpl->type == "file")
														<a href="/download/{{ $thisUpl->filename }}" target="_blank" title="Скачать"><img src="/img/{{ $thisUpl->filetype }}.png" alt="{{ $thisUpl->title }}" style="padding:35px;"></a>
													@elseif($thisUpl->type == "video")
														<a href="/uploads/{{ $thisUpl->filename }}.{{ $thisUpl->filetype }}" target="_blank" title="Просмотреть"><img src="/img/{{ $thisUpl->filetype }}.png" alt="{{ $thisUpl->title }}" style="padding:35px;"></a>
													@elseif($thisUpl->type == "image")
														<a href="/uploads/{{ $thisUpl->filename }}_full.{{ $thisUpl->filetype }}" target="_blank" title="Просмотреть"><img src="/uploads/{{ $thisUpl->filename }}.{{ $thisUpl->filetype }}" style="width:100px;border-radius:25px;padding:10px;" alt="{{ $thisUpl->title }}"></a>
													@endif
												</td><td style="text-align:left;vertical-align:middle;"><span style="font-size:12pt;">{{ htmlspecialchars($thisUpl->title) }}</span><br>{{ htmlspecialchars($thisUpl->description) }}</td>
											</tr>
										</table>
									</div>
									<div align="right" style="padding-right:35px;">{{ $thisUpl->created_at }} <img src="/img/calend.png" alt=""></div>
								</div>
							@endforeach
							@if($uploadsSummary>$itemsPerPage)
								<br>
								@if($pagination > 1)
								<input type="button" class="pull-left btn btn-sm btn-primary" value="« Предыдущие" onClick="javascript:pagination('back');">
								@endif
								@if($pagination < $pagesInSection)
								<input type="button" class="pull-right btn btn-sm btn-primary" value="Следующие »" onClick="javascript:pagination('forward');">
								@endif
							@endif
						@else
							В настоящий момент Ваша регистрация еще не была подтверждена администратором системы. Пожалуйста, повторите обращение несколько позже. Заявки на верификацию обрабатываются в ручном режиме. До завершения процесса верификации полный функционал системы будет недоступен.
						@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
